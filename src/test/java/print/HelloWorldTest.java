package print;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HelloWorldTest {
    @Test
    public void shouldGreetWhenInvoked(){
        HelloWorld helloWorld=new HelloWorld("Vipul");
        Assertions.assertEquals("Hello World! Vipul",helloWorld.greet());
    }
}
